import { Component } from '@angular/core';
import {Card, CardDeck} from "../ lib/CardDeck";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  CardDeck: CardDeck = new CardDeck();
  card: Card[] = [];

  constructor() {
    this.reset();

  }

  giveCard() {
    const cards = this.CardDeck.getCard();
    this.card.push(cards)
  }

  reset() {
    this.CardDeck = new CardDeck();
    this.card = this.CardDeck.getCards(2);
  }

  Score() {
    let score = 0;

    this.card.forEach(card => {
      score += card.getScore();
    });
    return score;
  }
}
