import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent {
 @Input() rank = '';
 @Input() suit = '';


  getClassname() {
    return 'card rank-' + this.rank.toLocaleLowerCase() + ' ' + this.suit;
  }

  getSymbol() {
    if(this.suit === 'diams')return '♦'
    if(this.suit === 'hearts')return '♥'
    if(this.suit === 'clubs')return '♣'
    if(this.suit === 'spades')return '♠'
      return;
  }


}


// diams: {name: 'diams', symbol: '♦'},
// hearts: {name: 'hearts', symbol: '♥'},
// clubs: {name: 'clubs', symbol: '♣'},
// spades: {name: 'spades', symbol: '♠'}
