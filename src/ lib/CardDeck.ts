const suits = ['diams', 'hearts', 'clubs', 'spades'];
const ranks = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];

export class Card {
  constructor(public rank: string, public suit: string) {}


  getScore(){
    if (this.rank === 'A') {
      return 11;
    }
    if (this.rank === 'J' || this.rank === 'Q' || this.rank === 'K'){
      return 10;
    }
    return parseInt(this.rank);
  }
}
export class CardDeck{
  cards: Card[] = [];
  constructor() {
    for (let rank of ranks) {
      for (let suit of suits) {
        const card = new Card(rank, suit);
        this.cards.push(card);
      }
    }
  }
  getCard() {
    const random = Math.floor(Math.random() *  this.cards.length);
    const result = this.cards.splice(random, 1);
    return  result[0];
  }

  getCards(howMany: number): Card[] {
    const playCards: Card[] = [];

    for (let i = 0; i < howMany; i++) {
      playCards.push(this.getCard())
    }
    return playCards;
  }
}




